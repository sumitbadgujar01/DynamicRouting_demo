var $routeProviderReference;  

MyApp.config(function ($routeProvider) {  
  $routeProviderReference = $routeProvider;  
});  

MyApp.run(['$route', '$http', '$rootScope',  
  function ($route, $http, $rootScope) {  
	  $http.get("../Script/User/routedata.json").success(function (data) {  
		  var iLoop = 0, currentRoute;  
		  for (iLoop = 0; iLoop < data.records.length; iLoop++) {  
			  currentRoute = data.records[iLoop];  
			  var routeName = "/" + currentRoute.KeyName;  
			  $routeProviderReference.when(routeName, {  
				  templateUrl: currentRoute.PageUrls  
			  });  
		  }  
		  $route.reload();  
	  });  
  }]);  